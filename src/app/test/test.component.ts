import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TabsetComponent } from 'ngx-bootstrap';
import { AudioRecordingService } from '../audio-recording.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})


export class TestComponent implements OnInit {

  @ViewChild('testTabs') testTabs: TabsetComponent;

  zoomOut: boolean = false;
  zoomIn: boolean = false;
  tabNumber: number = 0;
  option01: string;
  option02: any;
  option03: any;

  max: number = 5;
  rate: number = 1;

  question01: FormGroup;
  question02: FormGroup;
  question03: FormGroup;

  testDiagnose: string = "none";

  isRecording = false;
  recordedTime;
  blobUrl;

  constructor(
    private formBuilder: FormBuilder,
    private audioRecordingService: AudioRecordingService, private sanitizer: DomSanitizer) {

    this.audioRecordingService.recordingFailed().subscribe(() => {
      this.isRecording = false;
    });

    this.audioRecordingService.getRecordedTime().subscribe((time) => {
      this.recordedTime = time;
    });

    this.audioRecordingService.getRecordedBlob().subscribe((data) => {
      this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data.blob));
    });
  }


  ngOnInit() {
    this.question01 = this.formBuilder.group({
      radio: 'A'
    });
    this.question02 = this.formBuilder.group({
      radio: 'A'
    });
    this.question03 = this.formBuilder.group({
      radio: 'A'
    });

    this.testTabs.tabs[3].disabled = true;
    this.testTabs.tabs[4].disabled = false;

  }

  selectTab(tabId: number) {
    this.testTabs.tabs[tabId].active = true;
  }

  startTraining(tabId: number) {
    this.testTabs.tabs[tabId].disabled = false;
    this.testTabs.tabs[tabId].active = true;
  }

  zoom(option: string) {
    if(option == 'out') {
      console.log("out")
      this.zoomOut = true;
      this.zoomIn = false;
    }else{
      console.log("in")
      this.zoomOut = false;
      this.zoomIn = true;
    }
  }

  runSpeech(){
    console.log("talk")
  }

  runSelfTest(tabId: number) {
    this.testTabs.tabs[tabId].disabled = false;
    this.testTabs.tabs[tabId].active = true;

    this.option01 = this.question01.get('radio').value;
    this.option02 = this.question02.get('radio').value;
    this.option03 = this.question03.get('radio').value;

    if (this.option01 == 'A'){
      if (this.option02 == 'A'){
        if (this.option03 == 'A'){
          this.testDiagnose = "Anomic aphasia";
        }
        else{
          this.testDiagnose = "Conduction aphasia";         
        }
      }
      else{
        if (this.option03 == 'A'){
          this.testDiagnose = "Transcortical sensory aphasia";
        }
        else{
          this.testDiagnose = "Wernicke's aphasia";
        }
      }
    } else {
      if (this.option02 == 'A'){
        if (this.option03 == 'A'){
          this.testDiagnose = "Transcortical motor aphasia";
        }
        else{
          this.testDiagnose = "Broca's aphasia";
        }
      }
      else{
        if (this.option03 == 'A'){
          this.testDiagnose = "Mixed transcortical aphasia";
        }
        else{
          this.testDiagnose = "Global aphasia";
        }
      }
    }
  }

  startRecording() {
    if (!this.isRecording) {
      this.isRecording = true;
      this.audioRecordingService.startRecording();
    }
  }

  abortRecording() {
    if (this.isRecording) {
      this.isRecording = false;
      this.audioRecordingService.abortRecording();
    }
  }

  stopRecording() {
    if (this.isRecording) {
      this.audioRecordingService.stopRecording();
      this.isRecording = false;
    }
  }

  clearRecordedData() {
    this.blobUrl = null;
  }

  ngOnDestroy(): void {
    this.abortRecording();
  }

}
