import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { RatingModule } from 'ngx-bootstrap/rating';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { TestComponent } from './test/test.component';
import { NavComponent } from './nav/nav.component';
import { DonateComponent } from './donate/donate.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { AudioRecordingService } from './audio-recording.service';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { Globals } from './globals';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    UserAccountComponent,
    TestComponent,
    NavComponent,
    DonateComponent,
    FooterComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    RatingModule.forRoot(),
    ButtonsModule.forRoot(),
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  providers: [
    AudioRecordingService,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: '6LcERJ8UAAAAABZ-d7X-Ts1BCbFwT22o9zX-3ro4',
      } as RecaptchaSettings,
    },
    Globals
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
