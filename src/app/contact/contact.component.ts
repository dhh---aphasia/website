import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Mail } from '../mail';
import { Globals } from '../globals';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})

export class ContactComponent implements OnInit {

  messageForm: FormGroup;
  submitted = false;
  success = false;

  message: Mail = {};
  public errorMsg;
  donate = 'I like to support';

  contactMessage: string;
  contactEmail: string;
  formMessage: string;

  constructor(private formBuilder: FormBuilder,private http: HttpClient,private globals: Globals) {};

  ngOnInit() {
    this.messageForm = this.formBuilder.group({
      //   name: ['', Validators.required],
      //   lastname: ['',],
         email: ['', Validators.email],
         message: ['', Validators.required],
         recaptchaReactive: [null, Validators.required]
      });
   }

  submitForm() {

    this.sendMail();

    this.submitted = true;

    if (this.messageForm.invalid) {
        return;
    }

    this.success = true;

    let formData: FormData = new FormData();
    formData.append('email', this.contactEmail);
    formData.append('text', this.contactEmail);
    formData.append('firstName', 'not needed');
    formData.append('lastName', 'not needed');
    this.http.post('/api/v1/contactForm', formData)
      .subscribe(data => {
        console.log('Messages saved');
      },
      err => {
        console.log('Error uploading the message.');
        console.log(err);
      }
  );
  }

  sendMail(){
    this.formMessage = this.messageForm.controls.message.value;
    window.location.href = "mailto:" + this.globals.aphasiaMail + "&subject=Contact%20form&body=" + this.formMessage + " - Aphasia contact form";
  }


}
