import { Injectable, NgZone } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface RecordedAudioOutput {
  blob: Blob;
  title: string;
}

@Injectable()
export class AudioRecordingService {

  private stream;
  private recorder;
  private interval;
  private startTime;
  private _recorded = new Subject<RecordedAudioOutput>();
  private _recordingTime = new Subject<string>();
  private _recordingFailed = new Subject<string>();

  constructor(private http: HttpClient) { }

  getRecordedBlob(): Observable<RecordedAudioOutput> {
    return this._recorded.asObservable();
  }

  getRecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }

  recordingFailed(): Observable<string> {
    return this._recordingFailed.asObservable();
  }


  startRecording() {

    if (this.recorder) {
      // It means recording is already started or it is already recording something
      return;
    }

    this._recordingTime.next('00:00');
    navigator.mediaDevices.getUserMedia({ audio: true }).then(s => {
      this.stream = s;
      this.record();
    }).catch(error => {
      this._recordingFailed.next();
    });

  }

  abortRecording() {
    this.stopMedia();
  }

  private record() {

    this.recorder = new RecordRTC.StereoAudioRecorder(this.stream, {
      type: 'audio',
      mimeType: 'audio/webm'
    });

    this.recorder.record();
    this.startTime = moment();
    this.interval = setInterval(
      () => {
        const currentTime = moment();
        const diffTime = moment.duration(currentTime.diff(this.startTime));
        const time = this.toString(diffTime.minutes()) + ':' + this.toString(diffTime.seconds());
        this._recordingTime.next(time);
      },
      1000
    );
  }

  private toString(value) {
    let val = value;
    if (!value) {
      val = '00';
    }
    if (value < 10) {
      val = '0' + value;
    }
    return val;
  }

  stopRecording() {
    if (this.recorder) {
      this.recorder.stop((blob) => {
        if (this.startTime) {
          const mp3Name = encodeURIComponent('audio_' + new Date().getTime() + '.mp3');
          this.stopMedia();
          console.log('try upload audio file...');
          this.uploadBlob(blob);
          this._recorded.next({ blob: blob, title: mp3Name });
        }
      }, () => {
        this.stopMedia();
        this._recordingFailed.next();
      });
    }

    // if (this.recorder) {
    //   this.recorder.stop().getMp3().then(([buffer, blob]) => {
    //     console.log(buffer, blob);
    //     const file = new File(buffer, 'audioworkout.mp3', {
    //       type: blob.type,
    //       lastModified: Date.now()
    //     });
    //     this.uploadBlob(file);
    //   });
    // }
    // this.stopMedia();
    // this._recordingFailed.next();
  }

  private stopMedia() {
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval);
      this.startTime = null;
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
        this.stream = null;
      }
    }
  }

  // recorder.stop().getMp3().then(([buffer, blob]) => {
  //   console.log(buffer, blob);
  //   const file = new File(buffer, 'music.mp3', {
  //     type: blob.type,
  //     lastModified: Date.now()
  //   });

  private uploadBlob(blob: any) {
    const httpHeaders = new HttpHeaders({
      //'Content-type': 'application/x-www-form-urlencoded'
      'Accept': 'application/json'
    });
    this.http.get('/api/v1/getAllExcercises?page=1&pageSize=20', { headers: httpHeaders})
    .subscribe(data => {
      const exerciseId = data[0].id;
      console.log(exerciseId);

      let formData:FormData = new FormData();
      formData.append('audioFile', blob); // , blob.name

      // const httpHeaders = new HttpHeaders({
      //   //'Content-type': 'application/x-www-form-urlencoded'
      //   'Content-Type': 'multipart/form-data',
      //   'Accept': 'application/json'
      // });

      //this.http.post('/api/v1/saveSpeechWorkout', formData, { headers: httpHeaders})
      this.http.post('/api/v1/saveSpeechWorkout', formData,
        {
          params: {'exerciseId': exerciseId}
          // headers: new HttpHeaders({
          //   'Content-Type': 'multipart/form-data',
          //   'Accept': 'application/json'
          // })
        })
        .subscribe(data => {
          console.log('got audio file ', data)
        },
        err => {
          console.log('Error uploading the audiofile not logged');
          console.log(err);
        }
      );

      console.log('User data:');
      console.log(this);
    },
    err => console.log('User not logged'), // err => console.log(err),
  );

  }
}
