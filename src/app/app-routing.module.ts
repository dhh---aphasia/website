import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { TestComponent } from './test/test.component';
import { DonateComponent } from './donate/donate.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'user-account', component: UserAccountComponent },
  { path: 'test', component: TestComponent},
  { path: 'donate', component: DonateComponent},
  { path: 'contact', component: ContactComponent},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
