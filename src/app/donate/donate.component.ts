import { Component, OnInit } from '@angular/core';
import { Globals } from '../globals';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss']
})
export class DonateComponent implements OnInit {

  small_love: boolean;
  medium_love: boolean;
  big_love: boolean;
  fundGoal: string = "€ 5.000,-";
  fundAmount: string = "- fill in amount -";
  formMessage: string;

  constructor(private globals: Globals) { }

  ngOnInit() {
  }

  toggleAnswer(toggle){
    switch (toggle) {
      case "small_love":
        this.small_love = true;
        this.fundAmount = "5";
      break;
      case "medium_love":
        this.medium_love = true;
        this.fundAmount = "10"
      break;
      case "big_love":
        this.big_love = true;
        this.fundAmount = "50"
      break;
    }
  }
  
  sendMail(){
    this.formMessage = "I would like to donate: € " + this.fundAmount;
    window.location.href = "mailto:" + this.globals.aphasiaMail + "&subject=Donate%20form&body=" + this.formMessage + " - Aphasia donate form";
  }


}
